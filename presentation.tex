\documentclass[xetex]{beamer}
\usepackage{fontspec}
\usepackage{csquotes}
\usepackage{polyglossia}
\usepackage{tabularx}
\usepackage{listings}
\usepackage{listings-golang}
\usepackage{protobuf/lang}
\usepackage[style=numeric, hyperref=true]{biblatex}
\usepackage{hyperref}

\setmainlanguage[spelling=new,babelshorthands=true]{german}

\setmainfont[Ligatures=TeX]{Source Sans Pro}
\setsansfont[Ligatures=TeX,Scale=MatchLowercase]{Source Sans Pro}
\setmonofont[Ligatures=TeX,Scale=MatchLowercase]{DejaVu Sans Mono}

\addbibresource{presentation.bib}
\nocite{*}

\title{Microservices in Go}
\subtitle{Ein Kurztripp durch die Lande Go}
\lecture[EwKS'18]{Entwurfsmuster und komponentenbasierte Systeme 2018}{EwKS'18}
\author[Kaupe \& Suhren]{Sebastian N. Kaupe \& Stefan Suhren}

\usetheme{FHDO}
\setboolean{show-section-titles}{true}
\setboolean{show-subsection-titles}{true}
\setboolean{show-subsubsection-titles}{true}

\setbeamertemplate{caption}[numbered]

\hypersetup{
	colorlinks,
	citecolor=FHOrange,
	linkcolor=black
}

\definecolor{gray}{rgb}{.5,.5,.5}
\definecolor{mediumgray}{rgb}{.7,.7,.7}
\definecolor{lightgray}{rgb}{.9,.9,.9}
\definecolor{darkgreen}{rgb}{0,.3,0}
\definecolor{mediumgreen}{rgb}{0,.5,0}
\definecolor{lightgreen}{rgb}{.4,.8,.4}
\definecolor{darkpurple}{rgb}{.5,0,1}
\definecolor{orange}{rgb}{1,.4,.1}

\lstset{
	basicstyle=\ttfamily\small,
	breaklines=true,
	keywordstyle=\color{orange},
	stringstyle=\color{gray},
	commentstyle=\color{mediumgreen},
	tabsize=2,
	showstringspaces=false,
	frame=leftline,
	rulecolor=\color{mediumgray},
	numbers=left,
	stepnumber=1,
	numberstyle=\scriptsize\color{mediumgray},
	captionpos=b,
	language=Golang
}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\section{Eine kurze Einführung in Go}
\begin{frame}[label=toc]
	\frametitle{Inhalt}
	\tableofcontents[currentsection]
\end{frame}

\begin{frame} 
\frametitle{Go?} 
Go ist eine von Google entwickelte Programmiersprache.
\begin{block}{Ziele von Go}
	\begin{itemize}
		\item Einfach \& schnell zu lernen
		\item Kurze Buildzyklen
		\item Schnell in der Ausführung
		\item Fehlertolerant
		\item Für nebenläufige Programmierung geeignet
	\end{itemize}
\end{block}
\end{frame}

\begin{frame}{Go in Fakten}
\begin{table}
	\begin{tabular}{l|l}
		\textbf{Syntax} & C-Familie, Oberon-Einfluss \\
		\textbf{Ausführungsart} & Nativ \\
		\textbf{Speichermanagement} & GC \\
		\textbf{Paradigmen} & Prozedural, objekt-orientiert \\
	\end{tabular}
	\caption{Eigenschaften von Go}
\end{table}
\end{frame}

\subsection{Syntax}
\begin{frame}{Syntax}
Was folgt ist eine kurze Einführung in Grundzüge und interessante Aspekte von Go, keine komplette Übersicht.

\vspace{5mm}

Für eine komplette(re) Einführung in Go gibt es folgende Ressourcen direkt von den Sprachschöpfern:

\vspace{5mm}

\begin{tabularx}{\paperwidth}{lll}
	\href{https://tour.golang.org/}{A Tour of Go} & \href{https://golang.org/doc/code.html}{How to Write Go Code} & \href{https://golang.org/doc/effective_go.html}{Effective Go} \\
	{\small Übersicht aller Features}             & {\small Starthilfe für Go}                                    & {\small Idiomatisches Go}
\end{tabularx}
\end{frame}

\begin{frame}[label={fr:basic-types}]{Basisdatentypen}
\begin{table}
	\begin{tabular}{l|l|l}
		\textbf{Go}                   & \textbf{Standardwert} & \textbf{Java}             \\ \hline
		\texttt{bool}                 & \texttt{false}        & \texttt{boolean}          \\
		\texttt{int8 uint8 byte}      & \texttt{0}            & \texttt{byte}             \\
		\texttt{int16 uint16}         & \texttt{0}            & \texttt{short}            \\
		\texttt{int32 uint32 rune}    & \texttt{0}            & \texttt{int}              \\
		\texttt{int64 uint64}         & \texttt{0}            & \texttt{long}             \\
		\texttt{float32}              & \texttt{0.0}          & \texttt{float}            \\
		\texttt{float64}              & \texttt{0.0}          & \texttt{double}           \\
		\texttt{string}               & \texttt{"'"'}         & \texttt{java.lang.String} \\
		\texttt{complex64 complex128} & \texttt{0}            & ---                       \\
		\texttt{uintptr}              & \texttt{nil}          & ---
	\end{tabular}
	\caption{Basisdatentypen von Go im Vergleich zu Java (zzgl.\, \texttt{unsigned})}
\end{table}
\end{frame}

\begin{frame}[fragile]{Hello, Go!}
\begin{lstlisting}[caption={Hello-World-Beispiel in Go}]
package main

import (
  "fmt"
)

func main() {
  var x, y int = 1, 2
  z := 3 // var z int = 3
  const a int = 4
  fmt.Printf("%d %d %d %d\n", x, y, z, a)
}
\end{lstlisting}
\end{frame}

\begin{frame}{Hello, Go!}
\begin{itemize}
	\item Go-Entwicklung findet in einem Ordner des Dateisystems statt, dem sogenannten Workspace
	\item Die Umgebungsvariable \texttt{GOPATH} muss auf diesen zeigen
	\item Jede Go-Quelldatei beginnt mit einer Package-Deklaration
	\item Packages sind der Ordnerpfad unterhalb des src-Verzeichnisses im Workspace
	\item Packages können unter Angabe ihres Pfades importiert werden
	\item Packages der Go-Runtime verwenden kurze Pfade, externe üblicherweise die URL des Git-Repositories
\end{itemize}
\end{frame}

\begin{frame}{Hello, Go!}
\begin{itemize}
	\item Ausführbare Go-Programme beginnen ihre Ausführung in der Funktion \texttt{main} des gleichnamigen Packages
	\item Go unterstützt drei Arten der Variablendeklaration: Normal, kurz und konstant
	\begin{itemize}
		\item Die normale Deklaration beginnt mit \lstinline|var| und nennt explizit den Typ einer Variablen (Zeile 8)
		\item Die Kurzdeklaration mittels \lstinline|:=| ermittelt den Typ anhand der rechten Seite (Zeile 9)
		\item Konstanten werden mittels \lstinline|const| anstelle von \lstinline|var| deklariert (Zeile 10)
	\end{itemize}
	\item Hinter der Deklaration kann eine Initialisierung stattfinden
	\item Bei fehlender Initialisierung wird der Standardwert verwendet (siehe Folie \ref{fr:basic-types})
	\item Go unterstützt Mehrfachdeklaration und -initialisierung (siehe Zeile 8)
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Funktionen}
\begin{lstlisting}[caption={Go-Funktionen, NamedFrac wird exportiert}]
func frac(x, y int) int {
  return x / y
}

func NamedFrac(x, y int) (result int) {
  result = x / y
  return
}
\end{lstlisting}
\end{frame}

\begin{frame}{Funktionen}
\begin{itemize}
	\item Funktionen werden mittels \lstinline|func| deklariert
	\item Der Typ jedes Parameters wird hinter dem Namen deklariert; Parameter gleichen Typs können zusammengefasst und der Typ nur einmalig angegeben werden
	\item Der Rückgabetyp der Funktion folgt auf die Parameterliste
	\item Bei Funktionen ohne Rückgabewert wird die Angabe weggelassen
	\item Der Rückgabewert einer Funktion kann benannt werden (siehe \lstinline|NamedFrac|)
	\item Die Rückgabe erfolgt in diesem Fall über Zuweisung an den Namen und ein einfaches \lstinline|return|
	\item Nur Funktionen mit großem Anfangsbuchstaben werden exportiert und können außerhalb des Packages verwendet werden!
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Funktionen}
\begin{lstlisting}[caption={Go erlaubt mehr als einen Rückgabewert}]
func NextThree(x int) (int, int, int) {
  return x + 1, x + 2, x + 3
}

func main() {
  x := 1
  u, v, w := NextThree(x)
  fmt.Printf("%d %d %d\n", u, v, w)
}
\end{lstlisting}
\end{frame}

\begin{frame}{Funktionen}
\begin{itemize}
	\item Funktionen können mehr als einen Rückgabewert haben
	\item In diesem Fall wird die Liste der Typen der Rückgabewerte in Klammern gesetzt
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Funktionen}
\begin{lstlisting}[caption={Funktionen sind Werte. Quelle: \cite{go-fn}}]
import "math"

func compute(fn func(float64, float64) float64) float64 {
  return fn(3, 4)
}

func main() {
  hypot := func(x, y float64) float64 {
    return math.Sqrt(x*x + y*y)
  }
  fmt.Println(hypot(5, 12))

  fmt.Println(compute(hypot))
  fmt.Println(compute(math.Pow))
}

\end{lstlisting}
\end{frame}

\begin{frame}{Funktionen}
\begin{itemize}
	\item Funktionen sind in Go Werte, können also innerhalb anderer Funktionen deklariert sowie als Parameter und Rückgabewerte verwendet werden
	\item Bei der Verwendung als Parameter oder Rückgabewert folgt dabei auf \lstinline|func| direkt die Parameterliste (der Name steht vor dem \lstinline|func|), auf die wiederum der Rückgabewert folgt
	\item Bei der Deklaration anonymer Funktionen innerhalb einer anderen Funktion wird der Name erneut weggelassen (er steht links des Zuweisungsoperators), die Parameter jedoch benannt
	\item Anonyme Go-Funktionen sind Closures, können also Variablen und Parameter der sie umgebenden Funktion referenzieren
	\item Jedes Closure, das aus einer Funktion zurückgegeben wird, besitzt ihr eigenes Set an Variablen. Variablen der umgebenden Funktion werden \emph{nicht} geteilt
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Kontrollstrukturen}
\begin{lstlisting}[caption={\lstinline|if| mit eigenem Scope für Zwischenwerte in Go}]
func checkSum(x, y int) (result bool) {
  if z := Sum(x); z > y {
    result = true
  } else {
    result = false
  }
  return
}
\end{lstlisting}
\end{frame}

\begin{frame}{Kontrollstrukturen}
\begin{itemize}
	\item \lstinline|if| funktioniert wie in Java oder C, die Klammern werden jedoch weggelassen
	\item Zusätzlich kann man eine kurze Zuweisung vor der Bedingung vornehmen; diese ist nur innerhalb der \lstinline|if|-Anweisung sichtbar
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Kontrollstrukturen}
\begin{lstlisting}[caption={Go bietet nur die \lstinline|for|-Schleife}]
func Sum(x int) int {
  sum := 0
  for i := 0; i < x; i++ {
    sum += i
  }
  return sum
}

func while() {
  // Go hat keine while-Schleife
  x, y := 1, 10
  for x < y {
    x++
  }
}
\end{lstlisting}
\end{frame}

\begin{frame}{Kontrollstrukturen}
\begin{itemize}
	\item In Go gibt es keine \lstinline|while| oder \lstinline|do-while| Schleifen, nur \lstinline|for|
	\item Sie funktioniert wie in Java oder C, die Klammern um den Schleifenkopf werden jedoch weggelassen
	\item Das Äquivalent einer \lstinline|while|-Schleife ist eine \lstinline|for|-Schleife ohne Deklarations- und Modifikations-Anweisung. In diesem Fall werden die Semikola weggelassen (Zeile 12)
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Kontrollstrukturen}
\begin{lstlisting}[caption={\lstinline|switch|-Anweisung in Go. Quelle: \cite{go-switch}}]
func main() {
  fmt.Print("Go runs on ")
  switch os := runtime.GOOS; os {
  case "darwin":
    fmt.Println("OS X.")
  case "linux":
    fmt.Println("Linux.")
  default:
    fmt.Printf("%s.", os)
  }
}
\end{lstlisting}
\end{frame}

\begin{frame}{Kontrollstrukturen}
\begin{itemize}
	\item Go bietet auch das \lstinline|switch|-Statement
	\item Anders als in C oder Java bietet dieses kein Fallthrough; \lstinline|break| ist daher nicht notwending
	\item Die \lstinline|case|s können nicht nur Integer-Konstanten verwenden, sondern beliebige Werte
	\item Wie \lstinline|if| erlaubt auch \lstinline|switch| eine Kurzdeklaration vor der Bedingung, die nur innerhalb der Anweisung sichtbar ist
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Kontrollstrukturen}
\begin{lstlisting}[caption={Codeausführung bei Funktionsrückkehr mittels \lstinline|defer|}]
func outOfOrder() {
  fmt.Print("1")
  defer fmt.Println(" 2")
  fmt.Print(" 3")
}

func main() {
  outOfOrder()
  // Ausgabe: 1 3 2
}
\end{lstlisting}
\end{frame}

\begin{frame}
\begin{itemize}
	\item \lstinline|defer| ist ein neues Keyword, hinter dem ein Funktionsaufruf steht
	\item Dieser Aufruf wird ausgeführt, wenn die umgebende Funktion zurückkehrt
	\item Die Argumente der aufzurufenden Funktion werden hingegen sofort evaluiert
	\item Enthält eine Funktion mehr als eine \lstinline|defer|-Anweisung, so werden die Funktionen in umgekehrter Reihenfolge ihrer Deklaration ausgeführt
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Fehlerbehandlung, Teil 1}
\begin{lstlisting}[caption={Fehlerbehandlung basiert auf multiplen Rückgabewerten}]
import "strconv"

func main() {
  x, err := strconv.Atoi("text")
  if err != nil {
    fmt.Println("Not an integer!")
  }
}
\end{lstlisting}
\end{frame}

\begin{frame}{Fehlerbehandlung, Teil 1}
\begin{itemize}
	\item Fehlerbehandlung in Go wird über multiple Rückgabewerte realisiert; es gibt keine Exceptions
	\item Eine Funktion, deren Aufruf fehlschlagen kann, gibt mindestens zwei Ergebnisse zurück: Das Ergebnis des Aufrufs und einen Fehlerwert
	\item Trat kein Fehler auf, ist der Fehlerwert \lstinline|nil|
	\item Ansonsten ist der Rückgabewert beliebig und der Fehlerwert ungleich \lstinline|nil|
	\item Es ist Konvention, den zurückgegebenen Fehlerwert \lstinline|err| zu nennen
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Fehlerbehandlung, Teil 1}
\begin{lstlisting}[caption={Mittels \lstinline|panic| lassen sich Threads abbrechen, mit \lstinline|recover| eine Panik abfangen}]
func startPanic() {
  panic("Some data about the panic")
}

func recoverFromPanic() {
  defer func() {
    if r := recover(); r != nil {
      fmt.Printf("Recovered, data: %v\n", r)
    }
  }()
  startPanic()
  fmt.Println("Normal return (never happens)")
}
\end{lstlisting}
\end{frame}

\begin{frame}
\begin{itemize}
	\item Bei schwerwiegenden Fehlern, die auch vom Aufrufer nicht sinnvoll behandelt werden können, lässt sich mittels \lstinline|panic| der aktuelle Thread abbrechen
	\item \lstinline|panic| kann als Argument ein String übergeben werden, der den Grund der Panik beschreibt
	\item Eine \lstinline|panic| im Aufrufstack lässt sich mithilfe von \lstinline|recover| abfangen. Dazu ruft die aufrufende Funktion eine andere Funktion auf, die ihrerseits \lstinline|recover| aufruft.
	\item Ist der Rückgabewert von \lstinline|recover| ungleich \lstinline|nil|, so ist eine Panik aufgetreten. Der Rückgabewert ist dann der String, der \lstinline|panic| übergeben wurde
	\item \lstinline|recover| kann nur innerhalb einer Funktion verwendet werden, die mittels \lstinline|defer| aufgerufen wird. Das Beispiel verwendet dazu eine anonyme Funktion (beachte den Aufruf in Zeile 10!)
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Structs \& Objektorientierung}
\begin{lstlisting}[caption={Go bietet C-artige Strukturen statt Klassen}]
type double float64

type Vertex struct {
  X, Y, Z double
}

func CreateVertex(x, y, z double) *Vertex {
  v := Vertex{x, y, z}
  return &v
}
\end{lstlisting}
\end{frame}

\begin{frame}{Structs \& Objektorientierung}
\begin{itemize}
	\item Mittels des \lstinline|type|-Keywords lassen sich neue Typen deklarieren
	\item So lassen sich bspw. Aliase für die Basistypen anlegen (im Beispiel \lstinline|double| für \lstinline|float64|)
	\item Wie Funktionen werden nur Typen aus dem Package exportiert, deren Namen mit einem Großbuchstaben anfängt
	\item Go bietet keine Klassen, wie man sie aus Java oder C++ kennt
	\item Es gibt allerdings \lstinline|struct|s ähnlich des C-Konstrukts und eine Syntax für Methodenaufrufe auf diesen
	\item \lstinline|struct|s werden als Typ deklariert
	\item Die Felder eines \lstinline|struct|s sind nur dann von außerhalb des Packages sichtbar, wenn sie mit einem Großbuchstaben beginnen
\end{itemize}
\end{frame}

\begin{frame}{Structs \& Objektorientierung}
\begin{itemize}
	\item Eine neue Instanz eines \lstinline|struct|s wird wie in Zeile 8 gezeigt erzeugt
	\item Die Reihenfolge der Argumente bestimmt dabei die Zuweisung an die Felder des \lstinline|struct|s
	\item Alternativ können die Feldnamen explizit angegeben werden: \lstinline|Vertex\{X: 1.0, Y: 1.0\}|
	\item Felder, für die kein Wert bei der Erzeugung angegeben wurde, werden mit dem Standardwert belegt (siehe Folie \ref{fr:basic-types})
\end{itemize}
\end{frame}

\begin{frame}{Structs \& Objektorientierung}
\begin{itemize}
	\item Go unterstützt auch Zeiger bzw. Referenzen. Diese werden durch ein \lstinline|*| vor dem Typnamen gekennzeichnet
	\item Die Adresse einer Variablen bekommt man, wie auch in C, mittels des \lstinline|&|-Operators
	\item Es gibt keine Zeigerarithmetik
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Structs \& Objektorientierung}
\begin{lstlisting}[caption={Methoden werden über die Receiver-Notation deklariert}]
func (v *Vertex) Magnitude() float64 {
  return math.Sqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z)
}

func main() {
  v := CreateVertex(1, 2, 3)
  fmt.Println(v.Magnitude())
}
\end{lstlisting}
\end{frame}

\begin{frame}{Structs \& Objektorientierung}
\begin{itemize}
	\item Da Go klassenlose Objektorientierung bietet, müssen Methoden auf einem anderen Weg deklariert werden
	\item Go verwendet dazu die sogenannte Receiver-Notation
	\item Zwischen \lstinline|func| und dem Methodennamen wird dazu in Klammern der \emph{Receiver} deklariert
	\item Der Receiver ist der Name, unter dem das Objekt, auf dem die Methode aufgerufen wurde, innerhalb der Methode verfügbar ist
	\item Receiver müssen nicht ein Zeiger sein, sollten es jedoch; dies vermeidet unnötige Kopien und erlaubt es den Methoden, Felder des Receivers zu ändern (analog \emph{call by reference } in C)
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Structs \& Objektorientierung}
\begin{lstlisting}[caption={Polymorphie funktioniert mittels implizit implementierter Interfaces, die Methoden des Typs vorschreiben}]
type Vector interface {
  Magnitude() float64
}

func VectorMag(v Vector) float64 {
  return v.Magnitude()
}

func main() {
  v := CreateVertex(1, 2, 3)
  VectorMag(v)
}
\end{lstlisting}
\end{frame}

\begin{frame}{Structs \& Objektorientierung}
\begin{itemize}
	\item Polymorphie wird in Go mittels Interfaces umgesetzt
	\item Ein Interface ist ein Typ, der eine Anzahl von Methoden vorschreibt
	\item Ein Typ implementiert ein Interface, wenn er alle von diesem vorgeschriebenen Methoden implementiert
	\item Es gibt keine explizite Implementierung eines Interfaces; sind die vorgeschriebenen Methoden vorhanden, implementiert der Typ das Interface
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Fehlerbehandlung, Teil 2}
\begin{lstlisting}[caption={Eigene Fehlertypen lassen sich mittels des \lstinline|error|-Interfaces implementieren}]
type FooError struct {
  msg string
}

func (e *FooError) Error() string {
  return e.msg
}

func Foo(x int) (int, error) {
  if x > 10 {
    return x, nil
  }
  return 0, &FooError{"Some error message"}
}
\end{lstlisting}
\end{frame}

\begin{frame}{Fehlerbehandlung, Teil 2}
\begin{itemize}
	\item Das \lstinline|error|-Interface erlaubt es uns, eigene Fehlertypen umzusetzen
	\item Das Interface schreibt nur eine Methode vor: \lstinline|Error|, welche einen String mit der Fehlerbeschreibung zurückliefert
	\item Unserem \lstinline|struct| steht es daher frei, beliebige Felder zu besitzen; es muss nur sinnvoll in einen String umwandelbar sein
	\item Der Typ für den Fehlerwert ist dann nicht der Typ des \lstinline|struct|s, sondern \lstinline|error|
\end{itemize}
\end{frame}

\subsection{Nebenläufigkeit}
\begin{frame}[fragile]{Goroutinen}
\begin{lstlisting}[caption={Goroutinen sind leichtgewichtige Threads}]
func generate(n int) {
  for i := 0; i < n; i++ {
    time.Sleep(1000 * time.Millisecond)
    fmt.Println(i)
  }
  fmt.Println()
}

func main() {
  go generate(10)
}
\end{lstlisting}
\end{frame}

\begin{frame}{Goroutinen}
\begin{itemize}
	\item Goroutinen sind \enquote{leichtgewichtige}\cite{go-goroutine} Threads, die von der Go-Runtime verwaltet werden (\emph{green threads})
	\item Wie normale Threads teilen sie sich einen einzelnen Adressraum
	\item Goroutinen werden auf eine Anzahl nativer Threads gemappt (\emph{multiplexing}) und mit weniger Speicher augestattet, was die mögliche Anzahl von Goroutinen im Vergleich zu OS-Threads massiv erhöht \cite{go-doc-goroutines,go-faq-goroutines}
	\item Goroutinen werden mittels des Keywords \lstinline|go| gestartet und beenden sich automatisch nach Abarbeitung des an \lstinline|go| übergebenen Funktionsaufrufs
\end{itemize}
\end{frame}

\begin{frame}{Goroutinen}
\begin{itemize}
	\item Problem des Beispielcodes: Wir sehen keine Ausgabe, da alle Goroutinen bei Beendigung der main-Routine abgebrochen werden
	\item Lösung: \emph{Channel}
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Channels}
\begin{lstlisting}[caption={Channels übertragen Daten zwischen Goroutinen}]
func generate(n int, c chan int) {
  for i := 0; i < n; i++ {
    time.Sleep(1000 * time.Millisecond)
    fmt.Printf("%d ", i)
  }
  fmt.Println()
  c <- 0
}

func main() {
  c := make(chan int)
  go generate(10, c)
  _ = <-c
}
\end{lstlisting}
\end{frame}

\begin{frame}{Channels}
\begin{itemize}
	\item Channels erlauben es, Daten zwischen zwei Goroutinen auszutauschen
	\item Ein Channel wird mittels \lstinline|make(chan <TYPE>)| angelegt und ist bidirektional
	\item Auf Senderseite wird ein Wert mittels \lstinline|<-| in den Channel geschrieben (Zeile 7)
	\item Auf Empfängerseite wird derselbe Operator verwendet, um einen Wert aus dem Channel zu lesen (Zeile 14)
	\item Ist der Channel leer, blockiert die lesende Goroutine
	\item Ist der Channel voll, blockiert die schreibende Goroutine
	\item Gepufferte Channel für mehr als einen Wert können mittels \lstinline|make(chan <TYPE>, <CAPACITY>)| erzeugt werden
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Channels}
\begin{lstlisting}[caption={Automatisches Auslesen mittels \lstinline|range| und \lstinline|close|}]
func generate(n int, c chan int) {
  for i := 0; i < n; i++ {
    time.Sleep(100 * time.Millisecond)
    c <- i
  }
  close(c)
}

func main() {
  c := make(chan int)
  go generate(10, c)
  for i := range c {
    fmt.Printf("%d ", i)
  }
  fmt.Println()
}
\end{lstlisting}
\end{frame}

\begin{frame}{Channels}
\begin{itemize}
	\item Wird ein Channel nicht länger benötigt, kann er optional geschlossen werden (Zeile 6)
	\item Das Schließen eines Channels wird wie ein Fehler über einen zweiten Rückgabewert angezeigt: \lstinline|value, ok := <- c|
	\item Wurde der Channel geschlossen, ist der Wert von \lstinline|ok false|
	\item Dies erlaubt es, mittels \lstinline|range| automatisch über alle Werte eines Channels zu iterieren (Zeile 12). Die Schleife endet, sobald der Channel geschlossen wird
\end{itemize}
\end{frame}

\section{Microservices}
\againframe<2>{toc}

\begin{frame}{Mit Go-Paketen}
Folgende Standardelemente gibt es, um ohne Frameworks in \emph{Go} einen Microservice zu implementieren.
\begin{description}[encoding/json]
	\item[encoding/json] Kann verwendet werden, um Daten aus und in \emph{JSON} zu konvertieren.
	\item[net/http] Ein Web-Client und -Server, mit dem die Kommunikation zwischen anderen Services und Clients realisiert werden kann. Unterstützt sowohl HTTP/1.1, als auch HTTP/2.
\end{description}
\end{frame}

\begin{frame}{Router}
Um sich das Leben einfach zu machen, empfehlen sich noch folgende Pakete:
\begin{description}[github.com/gorilla/handlers]
	\item[github.com/gorilla/mux] Ein Router der es erlaubt einfach Endpunkte zu definieren.
	\item[github.com/gorilla/handlers] Einige fertige Handler für Logging, Kompression und ähnliches.
\end{description}
\end{frame}

\subsection{Beispielprojekt}
\begin{frame}{Idee}
Da wir beide RPGs spielen, war es naheliegend ein Inventar-System für eine einfache Verwaltung von Gegenständen und den beanspruchten Platz in verschiedenen Taschen zu schreiben.
\end{frame}

\begin{frame}{Übersicht}
Dafür haben wir das Projekt in folgende Teile zerlegt:
\begin{description}[ContainerService]
\item[CharacterService] Ein Service, der die verschiedenen Charaktere enthält.
\item[ContainerService] Verwaltet die Taschen aller Charaktere, sowie die Anzahl der enthaltenen Items.
\item[ItemService] Verwaltet alle Gegenstände, die Charaktere haben können.
\end{description}
Der Quellcode ist verfügbar unter
\begin{center}
	\href{https://gitlab.com/fhdo-ewks2018/}{https://gitlab.com/fhdo-ewks2018/}
\end{center}
\end{frame}

\subsection{RPC}
\begin{frame}{Übersicht}
Statt immer REST und JSON zu verwenden, hat sich gerade im Go-Umfeld \emph{RPC} und \emph{Protobuf} als Alternative für Backend-Services und mobile Clients durchgesetzt.
\end{frame}

\subsubsection*{Protobuf3}
\lstset{language=protobuf3} % Temporarily use protobuf3 highlighting
\begin{frame}{Protobuf}
\begin{quotation}{}
	\enquote{Protocol buffers are a flexible, efficient, automated mechanism for serializing structured data [\dots]}\cite{protobuf}
\end{quotation}
\end{frame}

\begin{frame}{Protobuf3}
\begin{itemize}
	\item Protobuf ist ein binäres Format
	\item Legt Wert auf einfache Auf- und Abwärtskompatibilität
	\item Das Format der Nachrichten wird in einer \emph{.proto}-Datei gespeichert
	\item Aus der \emph{.proto}-Datei wird der sprachspezifische Code generiert, der für den Zugriff benötigt wird
	\item Das Format ist auf beiden Seiten fest vordefiniert und bekannt
\end{itemize}
\end{frame}

\begin{frame}{Protobuf}
\begin{block}{Offiziell unterstützte Sprachen}
	\begin{itemize}
		\item C++
		\item C\#
		\item Go
		\item Java
		\item JavaScript
		\item Objective-C
		\item Python
		\item Ruby
	\end{itemize}
\end{block}
\end{frame}

\defverbatim[colored]{\lstcontainerproto}{
\begin{lstlisting}[caption={Beispiel-Antwort für einen ContainerService}]
	syntax = "proto3";
	package = container;

	message ContainerResponse {
		message Container {
			message item {
				string item_id = 1;
				uint32 amount = 2;
			}
			string container_id = 1;
			repeated Item items = 2;
		}
		repeated Container containers = 1;
	}
\end{lstlisting}
}

\begin{frame}[label={fr:protoexample}]{Beispiel}
\lstcontainerproto
\end{frame}

\begin{frame}{Protokollformat}
\begin{itemize}
	\item Der \lstinline|syntax|-Befehl erlaubt es uns, die Version 3 statt der Version 2 von Protobuf zu verwenden
	\item Wie in \emph{Go} können die Nachrichten mittels \lstinline|package| logisch gruppiert werden
	\item Jedem Feld wird eine nummerische ID für die Zuordnung zugewiesen
	\item Eine \emph{.proto}-Datei kann mehr als eine \lstinline|message|-Definition enthalten
\end{itemize}
\end{frame}

\againframe<2>{fr:protoexample}

\begin{frame}{Protokollformat}
Eine \lstinline|message|-Definition kann aus folgenden Teilen bestehen:
\begin{itemize}
	\item Einem primitiven Datentyp (Scalar Value Type), wie \lstinline|string| oder \lstinline|uint32|
	\item Einem \lstinline|enum|, welches einen von mehreren Werten enthält
	\item Einer gekapselten \lstinline|message|-Definition, um Untertypen zu definieren
\end{itemize}
\end{frame}

\begin{frame}{Protokollformat}
\begin{itemize}
	\item Um eine Liste von Werten zu speichern, kann einer Variable \lstinline|repeated| vorangestellt werden, wobei die Reihenfolge der Werte immer erhalten bleibt
	\item Damit ein ehemaliger Feldwert bzw. Name nicht noch einmal vergeben werden kann, sollten entfernte Felder mittels \lstinline|reserved| gesperrt werden
\end{itemize}
\end{frame}

\begin{frame}{Scalar Value Type}
\begin{itemize}
	\item Es gibt die bekannten Datentypen \lstinline|float|, \lstinline|double|, \lstinline|bool| und \lstinline|string|.
	\item Beliebige Bytefolgen können mit \lstinline|bytes| encodiert werden.
	\item Für ganzzahlige Werte gibt es mehrere verschiedene \emph{Integer} Varianten.
\end{itemize}
\end{frame}

\begin{frame}{Übersicht der Integer}
\begin{table}
	\begin{tabular}{l|c|p{0.3\linewidth}}
		\textbf{Datentypen}                        & \textbf{Größe}  & \textbf{Verwendung}       \\ \hline
		\lstinline|int32|, \lstinline|int64|       & Bis zu 4/8 Byte & Positive Zahlen           \\
		\lstinline|uint32|, \lstinline|uint64|     & Bis zu 4/8 Byte & Nur-positive Zahlen       \\
		\lstinline|sint32|, \lstinline|sint64|     & Bis zu 4/8 Byte & Negative Zahlen           \\
		\lstinline|fixed32|, \lstinline|fixed64|   &    4/8 Byte     & Große nur-positive Zahlen \\
		\lstinline|sfixed32|, \lstinline|sfixed64| &    4/8 Byte     & Große negative Zahlen
	\end{tabular}
\end{table}
\end{frame}

\begin{frame}{Integer}
\begin{itemize}
	\item Alle Integer gibt es als 32-Bit- und als 64-Bit-Variante.
	\item Alle Varianten von \emph{int} verwenden eine Codierung mit variabler Länge, während die \emph{fixed}-Varianten eine feste Länge haben.
	\item Die beiden \emph{int}-Typen können negative Werte enkodieren, sind jedoch besser für positive Zahlen.
	\item Für negative Zahlen sollten die beiden \emph{sint}-Typen verwendet werden.
	\item Die \emph{uint}- und \emph{fixed}-Typen sind ohne Vorzeichen.
	\item Die \emph{sfixed}-Typen haben Vorzeichen und feste Länge.
\end{itemize}
\end{frame}

\begin{frame}{Standardwerte}
Alle nicht explizit zugewiesenen Felder bekommen Standardwerte.
\begin{description}
	\item[String] Ein leerer String
	\item[Bytes] Eine leere Bytefolge
	\item[Bool] Der Wert \lstinline|false|
	\item[Nummern] Der Wert 0
	\item[Enum] Der erste definierte \lstinline|enum|-Wert (muss als 0 enkodiert werden)
	\item[Message] Das Feld ist nicht gesetzt (Der genaue Wert hängt von der Zielsprache ab)
	\item[Repeated] Eine leere Liste
\end{description}
\end{frame}

\begin{frame}{Standardwerte}
\begin{block}{Vorsicht!}
	Es kann nicht unterschieden werden, ob ein Standardwert verwendet wurde oder der Wert explizit darauf gesetzt wurde. Deswegen sollte bei der Wahl der Standardwerte besonderes aufgepasst werden.
\end{block}
\end{frame}

\defverbatim[colored]{\lstcontainerserviceproto}{
\begin{lstlisting}[caption={Beispiel-Servicedefinition für den ContainerService}]
	service ContainerService {
		rpc GetItems(ContainerRequest) returns (ContainerResponse);
	}
\end{lstlisting}
}

\begin{frame}{Beispiel}
\lstcontainerserviceproto
\end{frame}

\begin{frame}{Servicedefinition}
\begin{itemize}
	\item Eine \lstinline|service|-Definition besteht normalerweise aus einer Liste von \lstinline|rpc|-Anweisungen.
	\item Eine \lstinline|rpc|-Anweisung gibt dem Aufruf einen Namen, ein Anfrage-Objekt und ein Antwort-Objekt.
\end{itemize}
\end{frame}

\begin{frame}{Codegenerierung}
\begin{itemize}
	\item Um den entsprechenden Quelltext zu generieren, muss nur noch \emph{protoc} ausgeführt werden.
	\item Der \emph{protoc}-Befehl generiert standardmäßig keinen Transport.
\end{itemize}
\end{frame}

\lstset{language=Golang} % And back to go

\subsubsection*{RPC Allgemein}
\begin{frame}{RPC Allgemein}
Für den Transport kann eines der beiden RPC-Frameworks verwendet werden:
\begin{itemize}
	\item gRPC
	\item Twirp
\end{itemize}
\end{frame}

\subsubsection*{gRPC}
\begin{frame}{gRPC}
\begin{quotation}{}
	\enquote{gRPC is a modern open source high performance RPC framework that can run in any environment.}\cite{grpc}
\end{quotation}
\end{frame}

\begin{frame}{gRPC}
\begin{itemize}
	\item Binär-RPC via HTTP/2
	\item Bidirektionales Streamen mittels HTTP/2
	\item Stecksystem für Lastverteilung, Authentifizierung und mehr
	\item Umfangreiche Laufzeitumgebung (enthält einen eigenen HTTP/2-Webserver)
	\item Laufzeitumgebung und Compiler müssen gleiche Version haben
\end{itemize}
\end{frame}

\subsubsection*{Twirp}
\begin{frame}{Twirp}
\begin{quotation}{}
	\enquote{Twirp is a simple RPC framework built on protobuf.}\cite{twirp}
\end{quotation}
\end{frame}

\begin{frame}{Twirp}
\begin{itemize}
	\item Binär-RPC und JSON via HTTP/1.1
	\item Leichtgewichtig
	\item Generiert den gesamten Code in das Projekt (hat keine Laufzeitumgebung)
\end{itemize}
\end{frame}

\subsubsection*{}
\subsection{Frameworks}
\begin{frame}{Frameworks}
Es gibt viele Frameworks zur Microservice-Entwicklung in Go, die beiden Größten haben wir uns im Folgenden angeschaut.
\begin{itemize}
	\item Go Kit
	\item Micro
\end{itemize}
\end{frame}

\subsubsection*{Go Kit}
\begin{frame}{Go Kit}
\begin{quotation}{}
\enquote{Go kit will help you structure and build out your services, avoid common pitfalls, and write code that grows with grace.}\cite{go-kit}
\end{quotation}
\end{frame}

\begin{frame}{Go Kit}
\begin{itemize}
	\item Go Kit ist das ältere Framework von beiden.
	\item Gibt dem Microservice Struktur, ohne sich in das Ökosystem einzumischen.
	\item Bietet viele Pakete, die das Bauen von Mikroservices erleichtern sollen.
	\item Go Kit bietet mehr Kontrolle über die Service-Architektur.
\end{itemize}
\end{frame}

\subsubsection*{Micro}
\begin{frame}{Micro}
\begin{quotation}{}
	\enquote{Micro is a microservice ecosystem focused on simplifying distributed systems development.}\cite{micro}
\end{quotation}
\end{frame}

\begin{frame}{Micro}
\begin{itemize}
	\item Mehr eine Platform
	\item Plugins für z.B. DiscoveryServices, LoadBalancer und CuircutBreaker
	\item Micro ist ein \enquote{opinionated framework} und macht mehr Vorschriften bei der Architektur.
	\item Legt mehr Fokus auf die Geschäftslogik, statt der Kommunikation in verteilten Systemen.
	\item Gibt vernünftige Standards vor, die sich jederzeit ersetzen lassen.
\end{itemize}
\end{frame}

\subsubsection*{}
\section*{Literatur}
\begin{frame}[t]{Literaturhinweise}
\printbibliography[keyword={further}]
\end{frame}

\begin{frame}[t,allowframebreaks]{Quellen}
\printbibliography[notkeyword={further}]
\end{frame}
\end{document}